# README #

##This repository includes the (pseudo) code of the Beat Synchronization GWAS 
(Niarchou et al. "Genome-wide association study of musical beat synchronization demonstrates high polygenicity", https://doi.org/10.1101/836197).
We also provide the scripts and data to replicate our findings on the phenotype validation studies 1 and 2. 

### If you have questions about the code please contact ###

* Re the pseudocode: Maria Niarchou (maria.niarchou@vumc.org) and Peter Straub (peter.straub@vumc.org)
* Re the Phenotype validation studies, Genomic SEM and Multivariate GWAS: Dan Gustavson (daniel.e.gustavson@vumc.org)